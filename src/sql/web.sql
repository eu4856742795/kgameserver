-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2015 年 08 月 20 日 07:49
-- 服务器版本: 5.5.20
-- PHP 版本: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `web`
--

DELIMITER $$
--
-- 存储过程
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `selectPay`()
BEGIN 
select * from paylog;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `test`()
begin

end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `testp`()
begin
select * from paylog;
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- 表的结构 `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` char(255) NOT NULL COMMENT '帐号',
  `password` char(32) NOT NULL DEFAULT '0',
  `last_login_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tgnum` int(10) NOT NULL DEFAULT '0',
  `last_login_ip` varchar(15) NOT NULL,
  `dj` varchar(255) NOT NULL DEFAULT '''10000000''',
  `Scode` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_name` (`name`,`password`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=142 ;

--
-- 转存表中的数据 `account`
--

INSERT INTO `account` (`id`, `name`, `password`, `last_login_time`, `tgnum`, `last_login_ip`, `dj`, `Scode`) VALUES
(127, 'xiaobaobbq', 'xiaobaoabc', '2015-07-24 19:27:16', 0, '122.158.57.233', '22000', 'xiaobaoa'),
(128, 'aoaopkpkpl', 'aoaopkpkpl', '2015-07-24 20:02:10', 0, '122.156.209.91', '50000', 'aoaopkpkpl'),
(130, 'qiqi5210', 'xiaobaoabc', '2015-07-24 20:09:16', 0, '122.158.57.233', '0', 'xiaobaoa'),
(129, 'a123411', '123411', '2015-07-24 20:04:21', 0, '60.181.145.74', '50099', '1233411'),
(131, 'xiaobaoabc', 'xiaobaobbq', '2015-07-24 20:18:22', 0, '122.158.57.233', '50000', 'xiaobaoa'),
(132, 'sensne0234', 'sensen0234', '2015-07-24 20:20:58', 0, '122.156.209.91', '50000', 'sensen0234'),
(133, 'a498018414', 'sebseb0234', '2015-07-24 20:32:41', 0, '122.156.209.91', '0', 'sebseb0234'),
(134, 'xiaobao123', 'xiaobaoabc', '2015-07-24 20:34:03', 0, '122.158.57.233', '0', 'xiaobaoa'),
(135, 'vip5088', '85523196a', '2015-07-24 21:40:08', 0, '113.140.7.46', '0', '1990521'),
(136, 'nalan1', '123456', '2015-07-25 15:31:28', 0, '127.0.0.1', '0', '123456'),
(137, 'ywrs123', '123456', '2015-07-26 09:45:47', 0, '127.0.0.1', '0', '123456'),
(138, 'judingwan', 'judingwan', '2015-07-28 11:14:11', 0, '127.0.0.1', '''10000000''', '123456'),
(139, 'judingwan1', 'judingwan', '2015-07-28 11:26:38', 0, '127.0.0.1', '0', '123456'),
(140, 'z111111', '111111', '2015-07-29 04:00:07', 0, '127.0.0.1', '''10000000''', '111111'),
(141, 'q', 'q', '2015-07-29 05:16:46', 0, '127.0.0.1', '''10000000''', 'qqqqqq');

-- --------------------------------------------------------

--
-- 表的结构 `hunfu`
--

CREATE TABLE IF NOT EXISTS `hunfu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '游戏名称',
  `url` varchar(255) DEFAULT NULL,
  `home` varchar(255) DEFAULT NULL COMMENT '总站',
  `QQ1` varchar(255) DEFAULT '0' COMMENT '在线客服1',
  `QQ2` varchar(255) DEFAULT '0' COMMENT '在线客服2',
  `QQqun` varchar(255) DEFAULT NULL COMMENT '玩家群链接',
  `onlineKF` varchar(255) DEFAULT NULL COMMENT '漂浮在线客服',
  `pay` varchar(255) DEFAULT NULL COMMENT '充值链接',
  `youqing` varchar(255) DEFAULT NULL,
  `weiduan` int(11) DEFAULT '0' COMMENT '微端',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `login_info`
--

CREATE TABLE IF NOT EXISTS `login_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `logintime` datetime DEFAULT NULL,
  `ticket` varchar(255) DEFAULT NULL,
  `onlineip` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_uid` (`userid`),
  KEY `tick_index` (`ticket`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `login_log`
--

CREATE TABLE IF NOT EXISTS `login_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) DEFAULT NULL,
  `uname` varchar(255) DEFAULT NULL,
  `logintime` datetime DEFAULT NULL,
  `onlineip` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `chr_index` (`uname`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `paylog`
--

CREATE TABLE IF NOT EXISTS `paylog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) DEFAULT NULL,
  `account` char(255) NOT NULL COMMENT '帐号',
  `paytime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(15) NOT NULL,
  `yuanbao` varchar(255) NOT NULL DEFAULT '0',
  `order_id` varchar(255) DEFAULT NULL,
  `fl` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_name` (`account`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `pay_info`
--

CREATE TABLE IF NOT EXISTS `pay_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `paynum` varchar(18) DEFAULT NULL,
  `paytouser` varchar(255) DEFAULT NULL,
  `paygold` int(11) DEFAULT NULL,
  `paymoney` int(11) DEFAULT NULL,
  `paytime` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `ticket` varchar(255) DEFAULT NULL,
  `chrname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_uid` (`paytouser`) USING BTREE,
  KEY `index_oid` (`paynum`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `pay_log`
--

CREATE TABLE IF NOT EXISTS `pay_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `paytouser` varchar(255) DEFAULT NULL,
  `paygold` int(11) DEFAULT NULL,
  `paymoney` int(11) DEFAULT NULL,
  `paytime` datetime DEFAULT NULL,
  `paynum` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_touser` (`paytouser`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `server`
--

CREATE TABLE IF NOT EXISTS `server` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `jieshao` varchar(255) DEFAULT NULL,
  `fqid` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(255) DEFAULT NULL,
  `duankou` varchar(255) DEFAULT NULL,
  `zy` varchar(255) DEFAULT NULL,
  `zz` varchar(255) DEFAULT NULL,
  `fcm` int(11) NOT NULL AUTO_INCREMENT,
  `opentime` varchar(255) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`fcm`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `server`
--

INSERT INTO `server` (`id`, `name`, `jieshao`, `fqid`, `ip`, `duankou`, `zy`, `zz`, `fcm`, `opentime`, `version`) VALUES
(1, '暗黑西游1区', '[火热开放]', 1, '127.0.0.1', '8001', 'http://xy.0459sf.com/res/', NULL, 1, '1433743200', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `spread`
--

CREATE TABLE IF NOT EXISTS `spread` (
  `tid` varchar(50) NOT NULL,
  `lid` varchar(50) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `islq` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `tuiguang`
--

CREATE TABLE IF NOT EXISTS `tuiguang` (
  `tgid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `duihuan` int(11) NOT NULL,
  PRIMARY KEY (`tgid`),
  UNIQUE KEY `tgid` (`tgid`),
  KEY `username_2` (`username`),
  KEY `tgid_2` (`tgid`),
  KEY `tgid_3` (`tgid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
