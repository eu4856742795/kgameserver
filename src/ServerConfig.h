/**
**���ط�����������
*qykings
**/

#ifndef __ServerConfig_h__
#define __serverconfig_h__


#include "./common/json/cJSON.h"

#include <stdio.h>
#include <iostream>

class ServerConfig
{
public:
	ServerConfig(void);
	~ServerConfig(void);

	int loadJson(const char *filename);

private:
	void decodeJsonStr(char *data);
};

#endif
