#include "fileOperate.h"

FileOperate::FileOperate()
{
}	 

FileOperate::~FileOperate()
{
	fclose(fp);
}


void FileOperate::writeFile(char* msg,const char* operateType)
{
	std::cout<<"operateType:"<<operateType<<std::endl;
}


FILE * FileOperate::readFile(char *filename,const char* operateType)
{
	fp=fopen(filename,operateType);

	if(fp==nullptr)
	{
		std::cout<<"no file:"<< filename<<std::endl;
		exit(0);
	}
	return fp;
}

