/***
*文件相关的操作
*qykings
**/

#ifndef __FILEOPERATE_H__
#define __FILEOPERATE_H__

#include <iomanip>
#include <fstream>

#include <stdlib.h>
#include <string>
#include <iostream>
							  
//  打开一个文件时，该文件必须已经存在， 且只能从该文件读出
#define readOnly "r"	

//以可读写方式打开文件，该文件必须存在。
#define readWrite "r+" 

// 读写打开一个二进制文件，允许读写数据，文件必须存在。
#define readByte "rb+"

 /*打开只写文件，若文件存在则文件长度清为0，
 即该文件内容会消失。若文件不存在则建立该文件。*/
#define writemodel "w" 

 /*打开可读写文件，若文件存在则文件长度清为零，
 即该文件内容会消失。若文件不存在则建立该文件。*/
#define  writeRead "w+" 

 
 /*以附加的方式打开只写文件。若文件不存在，则会建立该文件，
 如果文件存在，写入的数据会被加到文件尾，即文件原先的内容会被保留。
 （EOF符保留） 以附加的方式打开只写文件。若文件不存在，则会建立该文件，
 如果文件存在，写入的数据会被加到文件尾，即文件原先的内容会被保留。（EOF符保留）*/
#define  add "a"

/*以附加方式打开可读写的文件。若文件不存在，则会建立该文件，
如果文件存在，写入的数据会被加到文件尾后，即文件原先的内容会被保留。（原来的EOF符不保留） */ 
#define  addWriteRead "a+" //

#define  onlyWritByte "wb"// 只写打开或新建一个二进制文件；只允许写数据。
#define  readWriteByte "wb+"// 读写打开或建立一个二进制文件，允许读和写。
#define  addReadWriteByte "ab+"// 读写打开一个二进制文件，允许读或在文件末追加数据。

/*打开一个叫string的文件，a表示append,就是说写入处理的时候是接着原来文件已有内容写入，
不是从头写入覆盖掉，t表示打开文件的类型是文本文件，+号表示对文件既可以读也可以写。 */
#define  appendReadWrite "at+"// 

class FileOperate
{
public:
	FileOperate();
	~FileOperate();

	/**文件目录**/
	void setDirectory(char *url);
	/** 写文件**/			  
	void writeFile(char* msg,const char* operateType);


	FILE * readFile(char *filename,const char* operateType);

private:
	char* _directoryUrl;
	FILE *fp;
	int p;
};

#endif
