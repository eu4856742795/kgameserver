/**
*qykings
*用保存内存全局的数据
**/

#ifndef __DATAMGR_H__
#define __DATAMGR_H__
class DataMgr
{
public:
	static DataMgr* GetInstance();

	/**socket端口**/
	int port;

	/**serach user name in db
    *是否存在 
    *存在 0
    *其它反回-1
    **/
	int serachUserInDB(char *namestr);


private:
	DataMgr(void);
	~DataMgr(void);
	static DataMgr *_instance;

	int list[];
};

#endif