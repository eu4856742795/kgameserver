/**
*qykings
*用保存内存全局的数据
**/

#ifndef __DBMgr_H__
#define __DBMgr_H__

#include <stdio.h>
#include <iostream>

#include <string.h>

#include "../common/json/cJSON.h"
//#include "../MySqlCenter.h"

class DBMgr
{
public:
	static DBMgr* GetInstance();

	/**socket端口**/
	int port;

	char t_Account[128];

	char t_RoleName[128];

	/**serach user name in db
	*是否存在
	*存在 0
	*其它反回-1
	**/
	int serachUserInDB(char *namestr);

	int loadCof(char *filename);

private:
	DBMgr();
	~DBMgr();
	static DBMgr *_instance;

	void decodeJsonStr(char *text);
};

#endif
