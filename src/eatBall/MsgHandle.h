/***
*qykings
*socket消息处理
*
*增加这个方法方便以后做	跨平台
***/

#ifndef __MSG_HANDLE_H__
#define __MSG_HANDLE_H__
#include <stdio.h>
#include <iostream>

#include "../base.h"

#include "../utils/CByteArrays.h"

#include "./../net/SGameSocket.h"


//#include "./topPro/LoginLogic.h"
//#include "./topPro/RegisterLogic.h"

#define MSGTYPE_LOGIN 1

#define MSGTYPE_REGISTER 2

#define MSGTYPE_CHAT 3

class MsgHandle
{
public:
	static MsgHandle* GetInstance();

	//处理
	int decodeMsg(CByteArrays msg,int socketId);

	 

	int broadcast(CByteArrays *msg);
	 

	int sendMsg(int socketid,CByteArrays byt);

private:
	MsgHandle(void);
	~MsgHandle(void);

	static MsgHandle *_instance;
};

#endif
