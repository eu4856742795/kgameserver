/**
*qykings
**定义各种基础的东西
***/

#ifndef __BASE_H__
#define __BASE_H__

#include <string.h>

#ifdef WIN32
#else
#define SOCKET_ERROR -1

typedef int        SOCKET;

#include <netinet/in.h>  
#include <arpa/inet.h>

#endif


/**
*用于记录soket连接
**/
struct SGameSocketList_Str
{
	/**在链接中的下标**/
	int current;

	/**储存socket id***/
	int socketId;

	SGameSocketList_Str * last;

	SGameSocketList_Str *next;
};

/**
*用于socket消息
*传输的结构
**/
struct MsgData_Str{
	char msgid[10];
	char msgType[10];
	char msg[100];
};



#endif
