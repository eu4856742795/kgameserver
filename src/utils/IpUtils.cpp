#include "IpUtils.h"


IpUtils::IpUtils()
{
}

IpUtils::~IpUtils()
{
}

const char * IpUtils::getLocalIp()
{
	char buf[256] = {0};

	struct hostent *ph = 0;
	gethostname(buf, sizeof( buf ));

	std::string hostNmae = buf;//此处获得本机名称

	ph = gethostbyname(buf);

	const char *ip = inet_ntoa(*((struct in_addr *)ph->h_addr_list[0]));//此处获得本机IP
	return ip;
}
