/***
*qykings
*用于socket通信
*把数据类型转成char[]
***/

#ifndef __CByteArrays_h__
#define __CByteArrays_h__

#include "stdio.h"
#include "stdlib.h"
#include <string.h>

class CByteArrays
{
public:
	CByteArrays(int value =1024);
	~CByteArrays(void);

	int size();
	int clear();

	int setCurrentPosition(int p=0);
	int resetCurrentPosition();
	int getCurrentPosition();
	int addCurrentPosition( int value);

	/**写入地址**/
	char* getWritePosition();

	char* getData();

	/**
	*解析网络数据包时使用
	**/
    int setData(char* data);

public:
	//void writeData(const char* p_data, unsigned int u_len);
	//	void writeLengthAndData(const char* p_data, unsigned int u_len);
	void writeChar(char data);
	//void writeUChar(unsigned char data);
	void writeShort(short data);
	//void writeUShort(unsigned short data);
	void writeInt(int data);
	//	void writeUInt(unsigned int data);
	//	void writeLongLong(long long data);
	//	void writeULongLong(unsigned long long data);
	void writeFloat(float data);
	void writeDouble(double data);
	void writeString(char* data ,int size);///char[]数组
	//void writeLengthAndString(const char* p_data);

public:
	//	void readData(char* p_out_data, unsigned int u_len);
	char readChar();
	//	unsigned char readUChar();
	short readShort();
	//	unsigned short readUShort();
	int readInt();
	//	unsigned int readUInt();
	//long long readLongLong();
	//	unsigned long long readULongLong();
	float readFloat();
	double readDouble();
	char* readString(char* target,int size);

private:
	//创建多大的数组
	int creat(int value=1024);

	int init();

private:
	char *_pChar;
	int _size;

	///当前操作位置
	int _currentPosition;
};

#endif
