/*ip 通用处理方法
*qykings
*/
#ifndef __IpUtils_h__
#define __IpUtils_h__


#ifdef WIN32 
#include <WinSock2.h>
#else
#include <netdb.h>  
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif


#include <stdio.h>
#include <iostream>
class IpUtils
{
public:
	IpUtils();
	~IpUtils();

public:
	const char *  getLocalIp();

private:

};

#endif
