#include "CByteArrays.h"

CByteArrays::CByteArrays(int value)
{
	_pChar = nullptr;
	_size = value;

	creat(value);
}

CByteArrays::~CByteArrays(void)
{
	_pChar = nullptr;
}

int CByteArrays::creat(int value)
{
	_pChar	= new char[value];
	memset(_pChar,0,value);

	init();
	return 0;
}

int CByteArrays::init()
{
	_currentPosition = 0;

	return 0;
}

int CByteArrays::setCurrentPosition(int p)
{
	_currentPosition = p;
	return 0;
}

int CByteArrays::getCurrentPosition()
{
	return _currentPosition;
}

int CByteArrays::resetCurrentPosition( )
{
	_currentPosition=0;
	return 0;
}

char* CByteArrays::getWritePosition()
{
	return 	_pChar+_currentPosition;
}

int CByteArrays::addCurrentPosition( int value)
{
	_currentPosition+=value;
	return _currentPosition;
}

int CByteArrays::size()
{
	return _size;
}

char* CByteArrays::getData()
{
	return _pChar;
}

int CByteArrays::setData(char* data)
{
	clear();
	_pChar = data;
_size = readInt();
	return 0;
}

int CByteArrays::clear()
{
	free(_pChar);
	_pChar =NULL;

	_size = 0;
	_currentPosition = 0;

	return 0;
}

int CByteArrays::readInt()
{
	unsigned char temp[ sizeof(int)];

	memcpy(temp,_pChar+_currentPosition,sizeof(int));

	_currentPosition+= sizeof(int);

	int ret = ((int)(temp[0]))<<24 ;
	ret+= ((int)(temp[1]))<<16;
	ret+= ((int)(temp[2]))<<8 ;
	ret+= ((int)(temp[3]));
	return ret;
}

short CByteArrays::readShort()
{
	char temp[ sizeof(short)];
	memcpy(temp,_pChar+_currentPosition,sizeof(short));
	_currentPosition+= sizeof(short);
 
	short tmp = temp[0] << 4 | temp[1];
	return tmp;
}

char CByteArrays::readChar()
{
	char temp;
	memcpy(&temp,_pChar+_currentPosition,sizeof(char));
	addCurrentPosition(sizeof(char));
	return temp;
}

float CByteArrays::readFloat()
{
	char temp[ sizeof(float) ];
	memcpy(temp,_pChar+_currentPosition,sizeof(float) );
	_currentPosition+= sizeof(float) ;
	return *(float*)temp;
}

double CByteArrays::readDouble()
{
	char temp[ sizeof(double) ];
	memcpy(temp,_pChar+_currentPosition,sizeof(double));
	_currentPosition+= sizeof(double);

	return  *(double*) temp;
}

void CByteArrays::writeInt( int data )
{
 
  memcpy( _pChar+_currentPosition,&data,sizeof(int));
 
	_currentPosition+=sizeof(int);
}

void CByteArrays::writeShort( short data )
{
	memcpy( _pChar+_currentPosition,&data,sizeof(short));
	_currentPosition+=sizeof(short);
}

void CByteArrays::writeFloat(float data)
{
	memcpy(getWritePosition(),&data, sizeof(float));
	addCurrentPosition( sizeof(float) );
}

void CByteArrays::writeDouble( double data )
{
	memcpy(getWritePosition(),&data, sizeof(double) );
	addCurrentPosition(  sizeof(double) );
}

void CByteArrays::writeChar(char data)
{
	memcpy(getWritePosition(),&data,sizeof(char));
	addCurrentPosition( sizeof(char ));
}

void CByteArrays::writeString(char *data ,int size)
{
    writeShort(size);

	memcpy(getWritePosition(),data,size);
	addCurrentPosition( size );
}

char* CByteArrays::readString(char* target,int size)
{
	short len= readShort();
	if (size<len)
	{
		realloc(target,len);
	}

	memcpy(target,_pChar+_currentPosition, len );
	addCurrentPosition(len);

	return target;
}