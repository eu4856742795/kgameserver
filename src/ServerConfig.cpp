#include "ServerConfig.h"

#include "mgr/DataMgr.h"


ServerConfig::ServerConfig(void)
{
}


ServerConfig::~ServerConfig(void)
{
}



int ServerConfig::loadJson(const char *filename)
{
	FILE *f=fopen(filename,"rb");

	if(f==nullptr)
	{
		std::cout<< "no file:"<<filename<<std::endl;

		exit(0);
	}

	fseek(f,0,SEEK_END);
	long len=ftell(f);
	fseek(f,0,SEEK_SET);
	char *data=(char*)malloc(len+1);
	fread(data,1,len,f);
	fclose(f);
	decodeJsonStr(data);
	free(data);
	return 0;
}

void ServerConfig::decodeJsonStr(char *text)
{
	cJSON *json;

	json=cJSON_Parse(text);
	if (!json)
	{
		printf("mysqlcenter Error before: [%s]\n",cJSON_GetErrorPtr());
	}
	else
	{
		

		cJSON *c=cJSON_GetObjectItem(json,"port");
	  
		DataMgr::GetInstance()->port=c->valueint;
	}
}
