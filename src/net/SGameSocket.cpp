#include "SGameSocket.h"

#include <sstream>

SGameSocket* SGameSocket::_instance = new SGameSocket();
MsgCallFun SGameSocket::_callFun = NULL;

SGameSocket* SGameSocket::GetInstance()
{
	return _instance;
}

SGameSocket::SGameSocket(void)
{

}

SGameSocket::~SGameSocket(void)
{
}


int SGameSocket::clearSocketList(int socketId)
{

	SGameSocketList_Str *str = findSocket(socketId, _headSgameSocket_str);
	if (str){
		if (str->last)
		{

			str->last = str->next;


			delete str;


		}
		else
		{
			if (str->next)
			{
				_headSgameSocket_str = str->next;
				_headSgameSocket_str->last = NULL;
			}
			_headSgameSocket_str->socketId = 0;
		}



		return 0;
	}
	return -1;
}

SGameSocketList_Str* SGameSocket::findSocket(int socketId, SGameSocketList_Str *str)
{
	if (str)
	{
		if (str->socketId == socketId)
		{
			return str;
		}


		if (str->next != NULL)
		{
			findSocket(socketId, str->next);
		}
	}



	return nullptr;
}


void *SGameSocket::rec_data(void* serConn)
{
	SGameSocketList_Str *temp = (SGameSocketList_Str*)serConn;
	int tmpcon = temp->socketId;

	char receiveBuf[MSG_LEN] = { 0 };//接收




	while (1){
		memset(receiveBuf, 0, MSG_LEN);//重置
		int buflen = recv(tmpcon, receiveBuf, sizeof(receiveBuf), 0);

		if (buflen <= 0)
		{
			// 这里表示对端的socket已正常关闭
			std::cout << "buflen:%s\n" << buflen <<
				"\nsocket已正常关闭 " << std::endl;
			int  socketId =  tmpcon;
			_instance->clearSocketList(socketId);
			break;
		}
		else if (buflen > 0)
		{


			CByteArrays byt;
			byt.setData(receiveBuf);

			_callFun(byt,  tmpcon);

		}
	}
	return NULL;
}

int SGameSocket::creat(const char *ipstr, short portValue)
{
	//创建套接字 
  
    int err;

#ifdef WIN32
    WORD myVersionRequest;
	WSADATA wsaData;
	myVersionRequest = MAKEWORD(1, 1);
	err = WSAStartup(myVersionRequest, &wsaData);


	if (!err){
		printf("socket is open \n");
	}
	else{
		printf("ERROR:socket is not open!!!!\n");
		return 1;
	}


#endif
	//进一步绑定套接字
	int serSocket = socket(AF_INET, SOCK_STREAM, 0);//创建了可识别套接字

	//需要绑定的参数
    sockaddr_in addr;
	addr.sin_family = AF_INET;
    
#ifdef WIN32  
	addr.sin_addr.S_un.S_addr = inet_addr(ipstr);//ip地址
#else
    addr.sin_addr.s_addr = inet_addr( ipstr );

#endif

	addr.sin_port = htons(portValue);//绑定端口


	int ret = bind(serSocket, (sockaddr*)&addr, sizeof(addr));//绑定完成
	listen(serSocket, NUM_THREADS);//其中第二个参数代表能够接收的最多的连接数

	//////////////////////////////////////////////////////////////////////////
	//开始进行监听
	//////////////////////////////////////////////////////////////////////////
	sockaddr_in clientsocket;
	int len = sizeof( addr );

	int tmp = 0;

	_headSgameSocket_str = new SGameSocketList_Str();
	SGameSocketList_Str* _currentSgameSocket_str = _headSgameSocket_str;
	_currentSgameSocket_str->current = 0;
	_currentSgameSocket_str->last = NULL;
	_currentSgameSocket_str->next = NULL;


	while (1)
	{
     #ifdef WIN32  
		int serConn = accept(serSocket, (sockaddr*)&clientsocket, &len);//如果这里不是accept而是conection的话。。就会不断的监听
#else

        int serConn = accept(serSocket, (sockaddr*)&clientsocket,NULL);
#endif

		if (_currentSgameSocket_str->socketId > 0)
		{
			SGameSocketList_Str* temp = new SGameSocketList_Str;
			temp->last = NULL; //nullptr;
			temp->next = NULL; //nullptr;
			temp->current = 0;
			temp->socketId = 0;

			_currentSgameSocket_str->next = temp;
			temp->last = _currentSgameSocket_str;
			 

			_currentSgameSocket_str = temp;
		}
		tmp++;
		_currentSgameSocket_str->current=tmp;

      

		std::cout << "current client number:"<< _currentSgameSocket_str->current<<std::endl;
		std::cout << "tmp:"<<tmp<<std::endl;

		_currentSgameSocket_str->socketId = serConn;

		pthread_t ntid;
		if ((pthread_create(&ntid, NULL, rec_data, _currentSgameSocket_str)) != 0)
		{
			printf("creat pthread fialed");
			break;
		}

		
	}

	return 1;
}

int SGameSocket::sendMsg(int socketId, CByteArrays* byt)
{
	 std::cout << "sendMsg:" << "socketid:" << socketId << std::endl;

	int len = byt->size();
	send(socketId, byt->getData(), len, 0);





	/*
CByteArrays cby;
//cby.writeChar(84);
//cby.writeChar(6);
// cby.writeInt(9);
//  cby.writeDouble(963);

// cby.writeShort(9638);

//char tstr[69]={"ssssafddsfsqykings"};

//  cby.writeString(tstr,sizeof(tstr));

//send(socketId, cby.getData(), cby.size(), 0);

*/

	return 0;
}


int SGameSocket::broadcast(CByteArrays *str)
{

	broadCast_curretnStr = _headSgameSocket_str;
	handleBroadCast(broadCast_curretnStr, str);
	return 0;
}

int SGameSocket::handleBroadCast(SGameSocketList_Str* tmp, CByteArrays *str)
{
	if (!tmp)
	{
		return 0;
	}

	if (tmp->socketId > 0)
	{
		sendMsg(tmp->socketId, str);
	}

	if (tmp->next != NULL)
	{
		handleBroadCast(tmp->next, str);
	}

	return 0;
}

int SGameSocket::destroy()
{
#ifdef WIN32
	WSACleanup();//释放资源的操作
#endif
	return 0;
}





int SGameSocket::setCallFun(MsgCallFun fun)
{
	SGameSocket::GetInstance()->_callFun = fun;
	return 0;
}
