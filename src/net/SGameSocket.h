/***
*socket接收处理
*
***/

#ifndef __SGAMESOCKET_H__
#define __SGAMESOCKET_H__


#include <stdio.h>
#include <iostream>

#ifdef WIN32  
#include <WinSock2.h>
#include <Windows.h>     
#else

#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <pthread.h>
#endif

#include <stdlib.h>

#include <pthread.h>
#include <string>

#include "../base.h"

 

#include "./../utils/CByteArrays.h"
 

#define NUM_THREADS 100
#define MSG_LEN 100

//定义一个回调函数，返回为int, 参数为int,int
typedef int(*MsgCallFun)(CByteArrays cby, int);
 

class SGameSocket
{
public:
	static SGameSocket* GetInstance();

	SGameSocket(void);
	~SGameSocket(void);

	int creat( const char *ipstr, short portValue);

	int sendMsg(int socketId,CByteArrays* byt);

	//广播
	int broadcast(CByteArrays *str);

	int destroy();

	int clearSocketList(int  socketid);

	static int setCallFun(MsgCallFun fun);

	static MsgCallFun _callFun;

private:
	static SGameSocket *_instance;
	static void *rec_data(void* serConn);

//	SOCKET _list[ NUM_THREADS ];
    pthread_t  _pList[ NUM_THREADS ];

 
	SGameSocketList_Str *_headSgameSocket_str;

	//
    SGameSocketList_Str *broadCast_curretnStr;

    int handleBroadCast(SGameSocketList_Str* tmp, CByteArrays *str);

	//int clearSocketList(int socketId);
	static SGameSocketList_Str* findSocket(int socketId,SGameSocketList_Str* str);

	
};

#endif
